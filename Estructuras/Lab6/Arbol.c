
#include <stdio.h> 
#include <stdlib.h> 

struct Arbol 
{ 
     int numero; 
     struct Arbol* izquierda; 
     struct Arbol* derecha; 
}; 
  

struct Arbol* nuevo_nodo(int numero) 
{ 
     struct Arbol* Arbol = (struct Arbol*) 
     malloc(sizeof(struct Arbol)); 
     Arbol->numero = numero; 
     Arbol->izquierda = NULL; 
     Arbol->derecha = NULL; 
  
     return(Arbol); 
} 
  

void Imprimir(struct Arbol* Arbol) 
{ 
     if (Arbol == NULL) 
        return; 
  
     Imprimir(Arbol->izquierda); 
  
     Imprimir(Arbol->derecha); 

     printf("%d ", Arbol->numero); 
} 
  

  

  
int main() 
{ 
     struct Arbol *root  = nuevo_nodo(1); 
     root->izquierda             = nuevo_nodo(2); 
     root->derecha           = nuevo_nodo(3); 
     root->izquierda->izquierda     = nuevo_nodo(4); 
     root->izquierda->derecha   = nuevo_nodo(5);
     root->derecha->izquierda = nuevo_nodo(6);
     root->derecha->derecha = nuevo_nodo(7);
     printf(" Asi se ve el arbol\n");
     printf("        1     \n ");
     printf("    2     3   \n");
     printf("   4  5   6  7 ");

  

     printf("\nElementos sacados con post-orden \n"); 
     Imprimir(root); 
  
     getchar(); 
     return 0; 
} 
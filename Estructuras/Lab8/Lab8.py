def main():

    x=lista_enlazada()
    print("se añaden elementos")
    lista_enlazada.agregar_final(x, 2)
    lista_enlazada.agregar_final(x, 3)
    lista_enlazada.agregar_final(x, 4)
    lista_enlazada.agregar_final(x, 5)
    lista_enlazada.agregar_final(x, 6)
    lista_enlazada.agregar_inicio(x, 1)
    lista_enlazada.imprimir(x)
    print("reverse")
    lista_enlazada.reverse(x)
    lista_enlazada.imprimir(x)
    print("se imprime el valor del indice 2")
    y = lista_enlazada.buscar_indice(x, 2)
    print(y.valor)
    print("se limpia")
    lista_enlazada.limpiar(x)
    print("se agrega un 7")
    lista_enlazada.agregar_final(x, 7)
    lista_enlazada.imprimir(x)

class Nodo:

    def __init__(self,valor=None,siguiente=None):
    
        self.valor = valor
    
        self.siguiente = siguiente
        
    def actualizar_valor(valor_nuevo):

        self.valor = valor_nuevo
    

    def actualizar_siguiente(siguiente_nuevo):

        self.siguiente = siguiente_nuevo

        
        
class lista_enlazada:

    def  __init__(self): 
        
        self.cabeza = None

    def agregar_inicio(self,valor):

        temp = Nodo(valor)

        temp.siguiente = self.cabeza

        self.cabeza = temp

        del temp


    def agregar_final(self, valor):
        
        if not self.cabeza:
            self.cabeza = Nodo(valor)
            return
        curr = self.cabeza
        while curr.siguiente:
            curr = curr.siguiente
        curr.siguiente = Nodo(valor)


    def buscar_indice(self, i):
        temp = self.cabeza
        cont = 1 
        while cont != i:
            temp = temp.siguiente
            cont = cont+1
        return temp  


    def lista_len(self):
        temp = self.cabeza
        cont = 0 
        while(temp != None):
            cont = cont + 1
            temp = temp.siguiente
        return cont

    def limpiar(self):
        temp = self.cabeza
        if temp is None:
            print("\n La lista ya estaba vacia")
            
        while temp:
            self.cabeza = temp.siguiente
            temp = None
            temp = self.cabeza
        
    def reverse(self):
            
        temp = self.cabeza
        nodo_ant = None
        nodo_sig = None
            
        while temp:
            nodo_sig = temp.siguiente
            temp.siguiente = nodo_ant
            nodo_ant = temp
            temp = nodo_sig
        self.cabeza = nodo_ant


    def imprimir(self):

        temp = self.cabeza

        while(temp != None):
        
            print(temp.valor)

            temp = temp.siguiente



main()


/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Ejemplos Prácticos: Lista de Estudiantes
    Estudiante:Luis Alfaro Alfaro 2019057649
**********************************************************************/
//Definición de Macros, Esto se hace para no tener que cambiar los valores
// de todo el código sino que con cambiar el valor aqui se cambia en todo el codigo 

//Luis Alfaro Alfaro
//2019057649
#define LONGITUD_MAXIMA_NOMBRE 50 
#define LONGITUD_MAXIMA_CARNET 12

//Definición de Estructuras
// Se define nodo_estudiante
typedef struct nodo_estudiante
{
	int carnet;
	char *nombre;

	struct nodo_estudiante *ref_siguiente;// se le da un puntero a siguiente
}nodo_estudiante;

typedef struct lista_estudiantes  // Se define la estructura de la lista con un puntero al inicio y una cantidad de nodos de la lista
{
	nodo_estudiante *ref_inicio;
  
	int cantidad;
}lista_estudiantes;


//Definición de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe parámetros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento: 
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_estudiante* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: No recibe entradas
	Salidas: Al ser un void no retorna nada
	Funcionamiento: 
    - Si la lista está vacia, le almacena espacio en la memoria a un nuevo nodo 
      y hace que su referencia a inicio apunte a nulo
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: recibe un  puntero de tipo nodo_estudiante a la posicion de un nuevo nodo que se quiera agregar
	Salidas: Es un void por lo que no retorna nada
	Funcionamiento: 
    - Si la lista está vacia, inicializa la lista y continua su funcionamiento.
    - Cambia el puntero siguiente del nodo nuevo de manera que apunte al nodo que era anteriormente el inicio.
    - Cambia el puntero de inico de la lista de modo que apunte al nuevo nodo.
    - le añade uno a cantidad
-----------------------------------------------------------------------*/
void insertar_inicio(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	insertar_final
	Entradas: recibe un  puntero de tipo nodo_estudiante a la posicion de un nuevo nodo que se quiera agregar
	Salidas: Es void, no tiene return
	Funcionamiento: 
    - Si la lista está vacia:
       inicializa la lista y continua su funcionamiento.
    - Si la cantidad de la lista es 0 usa la funcion inicializar_lista();.
    -  Se declara un puntero de tipo nodo_estudiante temporal.
    -  Le da un espacio en la memoria.
    -  Hace que temporal apunte al inicio de la lista.
    -   Recorre la lista con un while hasta que el siguiente de temporal apunte a NULL.
    -  Cuando el temporal->siguiente sea Null hace que temporal->siguiente apunte a nuevo.
    -  La cantidad de la lista se incrementa en 1. 


-----------------------------------------------------------------------*/
void insertar_final(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: Un int indice.
	Salidas: Es un viod y no tiene un valor de return.
	Funcionamiento: 
    - Se declara un puntero de tipo nodo_estudiante temporal.
    - Le da un espacio en la memoria.
    - Hace que temporal apunte a NULL.
    - Se crea un int contador equivalente a 0.
    - Valida si la lista está vacia.
    - Valida que indice sea valido.

    - Se declara otra vez el temporal.
    - Si el indice es 0:
      -no se recorre la lista, simplemente el inicio de la lista apunta al siguiente de temporal. 
      - Se decrementa la cantidad de la lista en uno. 
      - Se libera el espacio de temporal.
    - Si no:
      - Se hace un while que recorre la lista
      - por cada vuelta del while el contador se incrementa en 1.
      - Cuando el contador == indice:
        - Temporal->siguiente se convierte en temporal->siguiente->siguiente.
        - La cantidad de la lista se decrementa en 1.



-----------------------------------------------------------------------*/
void borrar_por_indice(int indice);
 /*-----------------------------------------------------------------------
	buscar_por_indice
	Entradas: Un int indice
	Salidas: Retorna el nodo temporal creado
	Funcionamiento: 
    - Se declara un puntero de tipo nodo_estudiante temporal.
    - Le da un espacio en la memoria.
    - Hace que temporal apunte a NULL.
  
    - Valida si la lista está vacia.
    - Valida que indice sea valido.

  
    - Se crea un int contador equivalente a 0.
    - Se declara otra vez el temporal.
 

    - Se hace un while que recorre la lista hasta que el siguiente sea null.
    - por cada vuelta del while el contador se incrementa en 1.
    - Cuando el contador == indice:
      - Retorna Temporal 

-----------------------------------------------------------------------*/
nodo_estudiante* buscar_por_indice(int indice);

 /*-----------------------------------------------------------------------
	validar_carnets
	Entradas: un int con el carnet del nodo que se quiere comparar y un int del int comparador
	Salidas: Es un void, no tiene salidas
	Funcionamiento: 
    - Compara los 2 ints:
     -Si es valido printea un mensaje que lo confirma
     -Sino:
       -Printea un mensaje que dice que el carnet no es valido
-----------------------------------------------------------------------*/
void validar_carnets(int carnet_almacenado, int carnet_ingresado);
 /*-----------------------------------------------------------------------
  menu
	Entradas: No tiene
	Salidas: Es void, no tiene 
	Funcionamiento: 
    - Declara un tamaño máximo, los siguientes ints:opcion,indice y carnet. y un puntero de       tipo nodo_estudiante temporal
    - Imprime un menu con las opciones.
    - Captura la opcion del usuario
    - Con un swich entra en la opcion indicada por el usuario:
      - 0: Termina el programa
      - 1: Añade estudiante al final 
      - 2: Añade estudiante al principio 
      - 3: Valida un carnet con una posicion dada
      - 4: Borra un estudiante en una posicion dada.


  
-----------------------------------------------------------------------*/
void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas: Ninguna 
	Salidas:  Es void, no tiene salidas
	Funcionamiento: 
  - Simplemente llama a menu 
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: El tamaño maximo que le llega de la funcion que lo llama
	Salidas: buffer
	Funcionamiento:
    - Declara un puntero a un  caracter llamada buffer y un size_t llamado characters 
    - Se le da un espacio en la memora a buffer
    - Si buffer == null:
    - Error: No se le pudo dar campo en la memoria a buffer
    - Con la funcion getline() se le da un valor a characters por medio de una input del usuario
    - Retorna buffer  
-----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
	Entradas: El tamaño maximo que le llega de la funcion que lo llama
	Salidas: numerical_input
	Funcionamiento: 
    - Declara un int llamado numerical_input y le mete un valor usando la funcion                 get_user_input() y lo tranforma en un int usando la funcion atoi()
    - Retorna numerical_input
-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);
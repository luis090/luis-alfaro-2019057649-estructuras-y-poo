import java.util.ArrayList;
import java.util.Scanner;
import java.util.InputMismatchException;
public class Administrador {

	private String contraseña;
	
	public Administrador(String contraseña) {
		this.contraseña = contraseña;
	}

  
  public String getContraseña(){
    return contraseña;
  }

  public void setContraseña(String contraseña) {
    this.contraseña = contraseña;
  }

  public void verListaPolicias(ArrayList<Policia> Policias){
    for (int i =0; i<Policias.size(); i++){

      System.out.println(Policias.get(i));
      }
    }
  
  public void agregarPolicia(int id, String contraseña, String nombre,Departamento departamento){
    Policia policia = new Policia(id, contraseña, nombre);

    departamento.Policias.add(policia);

  }
  public void eliminarPolicia(ArrayList<Policia> Policias, Policia policia){
    Policias.remove(policia);
  }

  public void agregarAsignacion(Policia policia,String nombre, String horaInicio, String horaFin, boolean permanente){ 

      Asignacion asignacion = new Asignacion(nombre, horaInicio, horaFin, permanente);

      policia.asignaciones.add(asignacion);
      

  }
  public void eliminarAsignacion(ArrayList<Asignacion> asignaciones, Asignacion asignacion){
    asignaciones.remove(asignacion);
  }
}
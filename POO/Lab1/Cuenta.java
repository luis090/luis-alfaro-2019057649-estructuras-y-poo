import java.sql.Date;
import java.time.LocalDate;

public class Cuenta{
  
  private int id;
  private double balance;
  private double TasaDeInteresAnual;
  private LocalDate FechaDeCreacion;


  public Cuenta(){
    this.id = 0;
    this.balance =0;
    this.TasaDeInteresAnual = 0;
    this.FechaDeCreacion = FechaDeCreacion.now();
  }

  public Cuenta(int id, double balance){
  this.id = id;
  this.balance = balance;
  this.FechaDeCreacion = FechaDeCreacion.now();
  }

  public int getid() {
    return this.id;
  }

  public void setid(int id){
    this.id = id;
  }

  public double getbalance() {
    return this.balance;
  }

  public void setbalance(double balance){
    this.balance = balance;
  }

  public double getTasaDeInteresAnual() {
  return this.TasaDeInteresAnual;
  }

  public void setTasaDeInteresAnual(double TasaDeInteresAnual){
    this.TasaDeInteresAnual = TasaDeInteresAnual;
  }

  public LocalDate getFechaDeCreacion(){
  return this.FechaDeCreacion;

  }

  public double ObtenerTasaDeInteresMensual(double TasaDeInteresAnual){
    return TasaDeInteresAnual / 12 ;
  }

  public double CalcularInteresMensual(double balance, double TasaDeInteresAnual){
    return TasaDeInteresAnual * balance /100 /12;
  }
  public void retirarDinero(double balance, double cantidad){
    if (balance < cantidad){
      System.out.println("Error.Fondos insuficientes");}
    else 
      {this.balance = balance - cantidad;}
    }
  public void depositarDinero(double balance, double cantidad){
    this.balance = balance + cantidad;

  }

  
  



}
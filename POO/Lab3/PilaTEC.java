import java.util.ArrayList;
//Luis Alfaro 2019057649
public class PilaTEC extends ArrayList{


public PilaTEC(){
  super();
}

public boolean isEmpty() {
return super.isEmpty();
}

public int getSize() {
return super.size();
}

public Object peek() {
return super.get(getSize() - 1 );
}

public Object pop() {
Object o = super.get(getSize() - 1 ); super.remove(getSize() - 1 );
return o;
}

public void push(Object o) {
super.add(o);
}

@Override
public String toString() {
return "pila: " + super.toString();
}

}